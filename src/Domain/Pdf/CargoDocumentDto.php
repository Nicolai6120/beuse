<?php

namespace App\Domain\Pdf;

class CargoDocumentDto
{
    /* @var string */
    public $date;

    /* @var string */
    public $mandateId;

    /* @var string */
    public $requestId;

    /* @var string */
    public $customer;

    /* @var string */
    public $agent;

    /* @var string */
    public $contractor;

    /**
     * @param string $date
     */
    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    /**
     * @param string $mandateId
     */
    public function setMandateId(string $mandateId): void
    {
        $this->mandateId = $mandateId;
    }

    /**
     * @param string $requestId
     */
    public function setRequestId(string $requestId): void
    {
        $this->requestId = $requestId;
    }

    /**
     * @param string $customer
     */
    public function setCustomer(string $customer): void
    {
        $this->customer = $customer;
    }

    /**
     * @param string $agent
     */
    public function setAgent(string $agent): void
    {
        $this->agent = $agent;
    }

    /**
     * @param string $contractor
     */
    public function setContractor(string $contractor): void
    {
        $this->contractor = $contractor;
    }
}