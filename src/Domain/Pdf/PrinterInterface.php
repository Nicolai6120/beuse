<?php

namespace App\Domain\Pdf;

interface PrinterInterface
{
    public function getPath();

    public function setPath(string $path): void;

    public function generateFromHtml(string $html): void;

    public function outputToFile(?string $path = null): void;
}