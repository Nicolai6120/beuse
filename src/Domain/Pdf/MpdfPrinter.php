<?php

namespace App\Domain\Pdf;

use Mpdf\HTMLParserMode;
use Mpdf\Mpdf;
use Mpdf\MpdfException;
use Mpdf\Output\Destination;

class MpdfPrinter extends Printer
{
    /**
 * Generator constructor.
 * @param Mpdf $printer
 * @param string $projectDir
 * @throws MpdfException
 */
    public function __construct(Mpdf $printer, string $projectDir)
    {
        parent::__construct();

        $this->printer = $printer;
        $this->printer->shrink_tables_to_fit = 1;

        $stylesheet = file_get_contents($projectDir . '/public/css/print.css');
        $this->printer->WriteHTML($stylesheet, HTMLParserMode::HEADER_CSS);
    }

    /**
     * @param $html
     * @throws MpdfException
     */
    public function generateFromHtml($html): void
    {
        $this->printer->WriteHTML($html, HTMLParserMode::HTML_BODY);
    }

    /**
     * @param string|null $path
     * @throws MpdfException
     */
    public function outputToFile(?string $path = null): void
    {
        $this->printer->Output($this->path, Destination::FILE);
    }
}