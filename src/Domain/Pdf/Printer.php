<?php

namespace App\Domain\Pdf;

use Carbon\Carbon;

abstract class Printer implements PrinterInterface
{
    /* @var mixed */
    protected $printer;

    /* @var string */
    protected $path;

    /**
     * Printer constructor.
     */
    public function __construct()
    {
        $this->path = sprintf('storage/pdf/%s.pdf', Carbon::now()->format('Ymd_His'));
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    abstract public function generateFromHtml(string $html): void;

    abstract public function outputToFile(?string $path = null): void;
}