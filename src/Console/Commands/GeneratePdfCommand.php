<?php

namespace App\Console\Commands;

use App\Domain\Pdf\CargoDocumentDto;
use App\Domain\Pdf\PrinterInterface;
use Carbon\Carbon;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GeneratePdfCommand extends Command
{
    protected static $defaultName = 'pdf:generate';

    /* @var PrinterInterface */
    private $printer;

    /* @var CargoDocumentDto */
    private $dto;


    public function __construct(CargoDocumentDto $dto, PrinterInterface $printer)
    {
        $this->printer = $printer;
        $this->dto = $dto;

        // Для простоты DTO кофигурируется прямо в консольной команде.
        // В реальной жизни этот код лежал бы в классе GenerateCargoDocumentJob
        // и DTO конфигурировалась бы в месте постановки задачи в очередь

        $this->dto->date = Carbon::now()->toDateTimeString();
        $this->dto->mandateId = 777;
        $this->dto->requestId = 888;
        $this->dto->customer = 'ООО «Сила», ИНН 77777777 КПП 777777777, 105111, Москва, Рублевское шоссе,  д.32,к.2, оф.777';
        $this->dto->agent = 'ООО «Джи Системс», ИНН 77777777 КПП 777777777, 143355, Московская область, Балашихинский район, мкр. Янтарный, ул. Кольцевая, д.10, к.3, оф.2';
        $this->dto->contractor = 'ООО «Джи Системс», ИНН 77777777 КПП 777777777, 143355, Московская область, Балашихинский район, мкр. Янтарный, ул. Кольцевая, д.10, к.3, оф.2';

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Awesome PDF generator.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // также для простоты шаблон зашит (мог бы лежать в отдельной папке)

        $html = "
            <section>
                <table>
                    <tr>
                        <td>Geesys DTS</td>
                        <td>Поручние №{$this->dto->mandateId} к Заявке №{$this->dto->requestId}</td>
                        <td>{$this->dto->date}</td>
                    </tr>
                </table>
            </section>
            
            <section>
                <div class=\"text-center\">
                    <p>Реквизиты сторон</p>
                </div>
                <div class=\"bordered\">
                    <p><b>Заказчик:</b> {$this->dto->customer}</p>
                    <p><b>Агент:</b> {$this->dto->agent}</p>
                    <p><b>Исполнитель:</b> {$this->dto->contractor}</p>
                </div>
            </section>
        ";

        $this->printer->generateFromHtml($html);
        $this->printer->outputToFile();

        $output->writeln("Your PDF file was generated! ({$this->printer->getPath()})");
    }
}