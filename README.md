**with Docker (laradock)**

1. create new folder `beuse.test`
2. `cd beuse.test`
3. `git clone https://gitlab.com/Nicolai6120/beuse.git .`
4. `git clone https://github.com/Laradock/laradock.git laradock`
5. `cd laradock`
6. `cp env-example .env`
7. `docker-compose up -d nginx workspace`
8. `docker-compose exec workspace bash` (провалиться внутрь workspace контейнера)
9. `composer install`
10. `php bin/console pdf:generate`
11. go to the `./storage/pdf` folder

**wihout docker (PHP 7.2^ required)**

1. create new folder `beuse.test`
2. `cd beuse.test`
3. `git clone https://gitlab.com/Nicolai6120/beuse.git .`
4. `composer install`
5. `php bin/console pdf:generate`
6. go to the `./storage/pdf` folder


